<?php

class WhiteRabbit
{
    
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),
                $occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filepath)
    {
        $array = array();
        $file=fopen($filepath, "r") or exit("Unable to open file!");
        while (!feof($file))
        {
            $char = fgetc($file);
              
            if (preg_match('/^\p{L}+$/u', $char)){
                $lowerChar = strtolower($char);
                if(array_key_exists($lowerChar, $array)){
                   $array[$lowerChar]++;
                } else {
                   $array[$lowerChar] = 1; 
                } 
            } 
        } 
        fclose($file);
        return $array;
    } 
      
    
    /** 
     * Return the letter whose occurrences are the median. 
     * @param $parsedFile 
     * @param $occurrences 
     */ 
    private function findMedianLetter($parsedFile, &$occurrences) 
    { 
        $allOccurances = array_values($parsedFile);
        $medianOccurance = $this->findKth($allOccurances, floor(sizeof($allOccurances)/2));
        $occurrences = $medianOccurance;
        
        foreach($parsedFile as $key => $value){
           if($value == $medianOccurance){
               return $key;
           }
        }
    }
    
    private function findKth($A,$k){  
        $randomIndex = array_rand($A);
        $pivot = $A[$randomIndex];

        $LR = $this->split($A, $pivot);
        $L = $LR[0];   
        $R = $LR[1];

        if($k == sizeof($L)+1){
            return $A[$randomIndex];
        }
        if ($k <= sizeof($L)){
            return $this->findKth($L, $k);
        }
        if ($k > sizeof($L)){
            return $this->findKth($R, $k-(sizeof($L)+1));
        }        
    }    
    private function split($A, $pivot) {
        $L = array();
        $R = array();
        $LR = array();
        foreach ($A as $value) {
            if($value < $pivot){
               array_push($L, $value);
            }
            if ($value > $pivot){
                array_push($R, $value);
            }
        }
        array_push($LR, $L, $R);
        return $LR;
    } 
}
